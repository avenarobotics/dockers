#!/usr/bin/env bash
### every exit != 0 fails the script
set -e
set -u

echo "Updating web_debug_tool"

rm -rf /opt/avena/web_debug_tool
git clone -b $GIT_BRANCH --single-branch https://gitlab.com/avenarobotics/web_debug_tool /opt/avena/web_debug_tool