#!/bin/bash

### every exit != 0 fails the script
set -e
export GIT_BRANCH=develop

path=$PWD

sudo chown ${USER:=$(/usr/bin/id -run)}:$USER -R /opt/avena

echo "Configure ROS2"

echo "LD_LIBRARY_PATH=\$LD_LIBRARY_PATH:/opt/lib/lib" >> $HOME/.bashrc
echo "export RMW_IMPLEMENTATION=rmw_cyclonedds_cpp" >> $HOME/.bashrc
echo "export CYCLONEDDS_URI=file://$HOME/cyclonedds.xml" >> $HOME/.bashrc
echo "source /opt/ros/foxy/setup.bash" >> $HOME/.bashrc
echo "source ~/ros2_ws/install/local_setup.bash" >> $HOME/.bashrc
echo "source ~/ros2_ws/install/setup.bash" >> $HOME/.bashrc

cp cyclonedds.xml ~/cyclonedds.xml

source $HOME/.bashrc

sh ava01_core/avena/scenes.sh

sh ava01_core/avena/avena_ros2.sh

cd ~/ros2_ws/
colcon build --packages-select custom_interfaces get_real_cameras_data virtualscene

source $HOME/.bashrc

cd $path
# Coppelia camera plugins
sh ava01_core/avena/simExtControlSimROS2.sh
sh ava01_core/avena/simExtGetSimCamerasData.sh

exit 0
