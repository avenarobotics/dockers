# Dockers
[![pipeline status](https://gitlab.com/avenarobotics/dockers/badges/develop/pipeline.svg)](https://gitlab.com/avenarobotics/dockers/-/commits/develop)

Maintainer: grzegorz.pogorzelski@pomagier.info

For all instructions regarding dockers please see:  
[https://www.avena.world/start/ubuntu-20.04-setup](https://www.avena.world/start/ubuntu-20.04-setup)

 
