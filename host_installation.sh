#!/bin/bash

### every exit != 0 fails the script
set -e

# ROS2
sh ava01_core/setup/setup-ros2.sh

# Coppelia Camera
mkdir -p /opt/avena
cp ava01_core/CoppeliaSim_* /opt/avena/
sh ava01_core/setup/setup-coppelia.sh

# Azure Kinect
cp ava01_core/debs/* /tmp/
sh ava01_core/setup/setup-azure-kinect-driver.sh
