#!/usr/bin/env bash
set -e

apt update
apt install -y --no-install-recommends locales curl gnupg2 lsb-release

apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654
curl -sSL 'http://keyserver.ubuntu.com/pks/lookup?op=get&search=0xC1CF6E31E6BADE8868B172B4F42ED6FBAB17C654' | apt-key add -
sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
apt update

apt-get install -y --no-install-recommends \
    libgflags-dev

apt-get install -y --no-install-recommends \
    ros-noetic-ros-base \
    ros-noetic-ros-control \
    ros-noetic-ros-controllers \
    ros-noetic-robot-state-publisher \
    ros-noetic-rosparam-shortcuts \
    ros-noetic-xacro

apt-get clean -y
rm -rf /var/lib/apt/lists/*
