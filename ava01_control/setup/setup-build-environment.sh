#!/usr/bin/env bash
### every exit != 0 fails the script
set -e

echo "Install build environment components"

apt-get update 

apt-get install -y \
    build-essential \
    cmake \
    gettext \ 
    git \
    xsltproc 

apt-get install -y \
    libeigen3-dev

apt-get autoclean -y 
apt-get autoremove -y 
apt-get clean -y
rm -rf /var/lib/apt/lists/*