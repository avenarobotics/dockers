#!/usr/bin/env bash

exit_on_error () {
    if (( $2 > 0 ))
    then
        echo "APP:$1 ERROR CODE: $2"
        exit $2
    fi   
}

start=`date +%s`

. /opt/ros/foxy/setup.bash && \
cd  ~/ros2_ws && \
colcon build --packages-select custom_interfaces

. /opt/ros/noetic/setup.bash && \
. /opt/ros/foxy/setup.bash && \
. ~/ros_ws/install_isolated/setup.bash && \
. ~/ros2_ws/install/local_setup.bash && \


colcon build --symlink-install --packages-select ros1_bridge --cmake-force-configure
. ~/ros2_ws/install/local_setup.bash 
ros2 run ros1_bridge dynamic_bridge --print-pairs | grep custom_interfaces

echo "Done"

end=`date +%s`
runtime=$((end-start))

echo "Building time " $runtime "s "

exit 0