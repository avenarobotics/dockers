#!/usr/bin/env bash
### every exit != 0 fails the script
set -e
set -u

rm -rf ~/ros2_ws/src/avena_ros2_control
git clone -b $GIT_BRANCH --single-branch https://gitlab.com/avenarobotics/avena_ros2_control ~/ros2_ws/src/avena_ros2_control

exit 0
