#!/usr/bin/env bash

exit_on_error () {
    if (( $2 > 0 ))
    then
        echo "APP:$1 ERROR CODE: $2"
        exit $2
    fi   
}

mkdir -p /opt/avena/cli

source /opt/ros/foxy/setup.bash

start=`date +%s`

cd /root/install
echo "Updating ROS applications"

./avena_ros_control.sh & ret1=$!
./avena_ros2_control.sh & ret2=$!

wait $ret1; exit_on_error "avena_ros_control" $?
wait $ret2; exit_on_error "avena_ros2_control" $?

echo "Done"

end=`date +%s`
runtime=$((end-start))

echo "Building time " $runtime "s "

exit 0