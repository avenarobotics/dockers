#!/usr/bin/env bash
### every exit != 0 fails the script
set -e
set -u

rm -rf ~/ros_ws/src/avena_ros_control
git clone -b $GIT_BRANCH --single-branch https://gitlab.com/avenarobotics/avena_ros_control ~/ros_ws/src/avena_ros_control

exit 0
