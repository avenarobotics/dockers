#!/usr/bin/env bash

exit_on_error () {
    if (( $2 > 0 ))
    then
        echo "APP:$1 ERROR CODE: $2"
        exit $2
    fi   
}

start=`date +%s`

# setup
. /opt/ros/noetic/setup.bash && \
cd  ~/ros_ws && \
catkin_make_isolated --install

# mkdir -p /opt/avena/cli

# source /opt/ros/foxy/setup.bash

# start=`date +%s`

# cd /root/install
# echo "Updating ROS applications"

# ./avena_ros_control.sh & ret1=$!
# ./avena_ros2_control.sh & ret2=$!

# wait $ret1; exit_on_error "avena_ros_control" $?
# wait $ret2; exit_on_error "avena_ros2_control" $?

# setup
# source /opt/ros/noetic/setup.bash

# cd  ~/ros_ws

# /opt/ros/noetic/bin/catkin_make_isolated --install

# source /opt/ros/foxy/setup.bash
# cd  ~/ros2_ws
# colcon build --packages-select custom_interfaces
# source ~/ros2_ws/install/setup.bash

# colcon build --symlink-install --packages-select ros1_bridge --cmake-force-configure

# source ~/ros2_ws/install/setup.bash

# ros2 run ros1_bridge dynamic_bridge --print-pairs 
# # | grep custom_interfaces

echo "Done"

end=`date +%s`
runtime=$((end-start))

echo "Building time " $runtime "s "

exit 0