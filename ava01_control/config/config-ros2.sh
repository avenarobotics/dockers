#!/usr/bin/env bash
### every exit != 0 fails the script
set -e

echo "Configure ROS2"

echo "source ~/.bashrc_avena" >> $HOME/.bashrc

cp $HOME/tool/config/.bashrc_avena $HOME/.bashrc_avena

mkdir -p ~/ros2_ws/src
cd ~/ros2_ws
. $HOME/.bashrc
source /opt/ros/foxy/setup.bash
