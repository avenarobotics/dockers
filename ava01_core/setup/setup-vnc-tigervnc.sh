#!/usr/bin/env bash
### every exit != 0 fails the script
set -e

echo "Install some common tools for further installation"
apt-get update 

# apt-get install -y tigervnc-common tigervnc-scraping-server tigervnc-standalone-server tigervnc-viewer tigervnc-xorg-extension

apt-get clean -y
rm -rf /var/lib/apt/lists/*

# ln -s /usr/share/novnc/vnc_lite.html /usr/share/novnc/index.html
