#!/usr/bin/env bash
### every exit != 0 fails the script
set -e

echo "Install Kivy"
apt-get update 
  
apt-get install -y python3-kivy python3-pip

apt-get autoclean -y 
apt-get autoremove -y 
apt-get clean -y
rm -rf /var/lib/apt/lists/*

pip3 install kivymd
