#!/usr/bin/env bash
set -e

apt update
apt install -y --no-install-recommends locales curl gnupg2 lsb-release

wget --no-check-certificate -O - https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | apt-key add -
# curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | apt-key add -

sh -c 'echo "deb [arch=$(dpkg --print-architecture)] http://packages.ros.org/ros2/ubuntu $(lsb_release -cs) main" > /etc/apt/sources.list.d/ros2-latest.list'

apt update

apt install -y --no-install-recommends \
    python3-argcomplete \
    python3-colcon-argcomplete \
    python3-colcon-common-extensions \
    python3-pip \
    ros-foxy-ros-base

apt install -y --no-install-recommends \
    ros-foxy-cv-bridge \
    ros-foxy-example-interfaces \
    ros-foxy-image-transport \
    ros-foxy-pcl-conversions \
    ros-foxy-pcl-msgs

apt install -y --no-install-recommends \
    ros-foxy-rmw-cyclonedds-cpp

apt install -y --no-install-recommends \
    ros-foxy-angles \
    ros-foxy-joint-state-publisher \
    ros-foxy-robot-state-publisher \
    ros-foxy-xacro

apt install -y --no-install-recommends \
    ros-foxy-py-trees \
    ros-foxy-py-trees-ros \
    ros-foxy-py-trees-ros-interfaces \
    ros-foxy-py-trees-ros-viewer

apt install -y --no-install-recommends \
    ros-foxy-behaviortree-cpp-v3 \
    ros-foxy-kdl-parser \
    ros-foxy-ompl \
    ros-foxy-orocos-kdl \
    ros-foxy-rqt-gui \
    ros-foxy-rqt-gui-cpp \
    ros-foxy-rviz2

# apt install -y --no-install-recommends \
#     python3-matplotlib

pip3 install matplotlib
pip3 install file-read-backwards
pip3 install pybullet

echo "/opt/ros/foxy/opt/rviz_ogre_vendor/lib" >> /etc/ld.so.conf.d/rviz2.conf

# pip3 install -U argcomplete

apt-get clean -y
rm -rf /var/lib/apt/lists/*
