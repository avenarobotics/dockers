#!/usr/bin/env bash
### every exit != 0 fails the script
set -e

echo "Install VirtualGL"

cd /tmp/

apt update

export VIRTUALGL_VERSION=2.6.80 

apt install -y --no-install-recommends ./virtualgl_${VIRTUALGL_VERSION}_amd64.deb ./virtualgl32_${VIRTUALGL_VERSION}_amd64.deb

rm virtualgl_${VIRTUALGL_VERSION}_amd64.deb virtualgl32_${VIRTUALGL_VERSION}_amd64.deb

apt clean -y
rm -rf /var/lib/apt/lists/*