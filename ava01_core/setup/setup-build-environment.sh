#!/usr/bin/env bash
### every exit != 0 fails the script
set -e

echo "Install build environment components"

apt-get update 

apt-get install -y \
    build-essential \
    cmake \
    gettext \
    xsltproc 

apt-get install -y \
    libboost1.71-dev \
    libmysqlcppconn-dev \
    libopencv-dev \
    libpcl-dev \
    libyaml-cpp-dev \
    libzmq3-dev \
    nlohmann-json3-dev \
    mysql-client

# cd ~
# wget https://github.com/git/git/archive/v2.30.0.tar.gz
# tar -xzf v2.30.0.tar.gz
# cd git-2.30.0
# make prefix=/usr all
# make prefix=/usr install

# cd ~
# rm -rf v2.30.0.tar.gz
# rm -rf git-2.30.0

apt-get autoclean -y 
apt-get autoremove -y 
apt-get clean -y
rm -rf /var/lib/apt/lists/*