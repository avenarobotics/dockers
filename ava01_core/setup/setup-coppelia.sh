#!/usr/bin/env bash
### every exit != 0 fails the script
set -e
set -u

mkdir -p /opt/avena

cd /opt/avena

tar -xf CoppeliaSim_Edu_V4_1_0_Ubuntu20_04.tar.xz
tar -xf CoppeliaSim_Player_V4_1_0_Ubuntu20_04.tar.xz

rm CoppeliaSim_Edu_V4_1_0_Ubuntu20_04.tar.xz
rm CoppeliaSim_Player_V4_1_0_Ubuntu20_04.tar.xz

mv CoppeliaSim_Edu_V4_1_0_Ubuntu20_04/programming/ CoppeliaSim_Player_V4_1_0_Ubuntu20_04

rm -rf CoppeliaSim_Edu_V4_1_0_Ubuntu20_04/

cp -r CoppeliaSim_Player_V4_1_0_Ubuntu20_04 /opt/avena/coppelia_brain
cp -r CoppeliaSim_Player_V4_1_0_Ubuntu20_04 /opt/avena/coppelia_camera

rm -rf CoppeliaSim_Player_V4_1_0_Ubuntu20_04  