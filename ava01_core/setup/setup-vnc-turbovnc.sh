#!/usr/bin/env bash
### every exit != 0 fails the script
set -e

export TURBOVNC_VERSION=2.2.80

echo "Install TurboVNC"

cd /tmp/

apt-get update && apt-get install -y --no-install-recommends ./turbovnc_${TURBOVNC_VERSION}_amd64.deb

rm turbovnc_${TURBOVNC_VERSION}_amd64.deb

apt-get clean -y
rm -rf /var/lib/apt/lists/*
