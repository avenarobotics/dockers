#!/usr/bin/env bash
### every exit != 0 fails the script
set -e

echo "Install NoVNC"

export NOVNC_VERSION=1.2.0

curl -fsSL https://github.com/novnc/noVNC/archive/v${NOVNC_VERSION}.tar.gz | tar -xzf - -C /opt
mv /opt/noVNC-${NOVNC_VERSION} /opt/noVNC
ln -s /opt/noVNC/vnc.html /opt/noVNC/index.html
git clone https://github.com/novnc/websockify /opt/noVNC/utils/websockify

# apt-get update 

# apt-get install -y net-tools novnc

# apt-get clean -y
# rm -rf /var/lib/apt/lists/*

# ln -s /usr/share/novnc/vnc_lite.html /usr/share/novnc/index.html
