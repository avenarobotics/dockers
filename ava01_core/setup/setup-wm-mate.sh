#!/usr/bin/env bash
### every exit != 0 fails the script
set -e

echo "Install MATE components"
apt-get update 

apt-get install -y ubuntu-mate-core

apt-get autoclean -y 
apt-get autoremove -y 
apt-get clean -y
rm -rf /var/lib/apt/lists/*
