#!/bin/bash
set -e

trap "echo TRAPed signal" HUP INT QUIT KILL TERM

echo "root:$VNCPASS" | chpasswd

mkdir -p ~/.vnc
echo "$VNCPASS" | /opt/TurboVNC/bin/vncpasswd -f >~/.vnc/passwd
chmod 0600 ~/.vnc/passwd

printf "3\nn\nx\n" | /opt/VirtualGL/bin/vglserver_config

# for DRM in /dev/dri/card*; do
#   if /opt/VirtualGL/bin/eglinfo "$DRM"; then
#     export VGL_DISPLAY="$DRM"
#     break
#   fi
# done

[ -S /tmp/.X11-unix/X10 ] && rm /tmp/.X11-unix/X10
[ -f /tmp/.X10-lock ] && rm /tmp/.X10-lock

/opt/TurboVNC/bin/vncserver :10 -wm "mate-session" -geometry "${SIZEW}x${SIZEH}" -depth "$CDEPTH" -vgl -noreset -alwaysshared &

/opt/noVNC/utils/launch.sh --vnc localhost:5910 --listen 6901 &

pulseaudio --start

echo "Session Running. Press [Return] to exit."
read
