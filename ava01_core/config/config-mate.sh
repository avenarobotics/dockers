#!/usr/bin/env bash
### every exit != 0 fails the script
set -e

echo "Configure MATE components"

dbus-launch dconf load / < /root/tool/config/dconf.dump
