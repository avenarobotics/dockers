#!/usr/bin/env bash
### every exit != 0 fails the script
set -e
set -u

rm -rf ~/ros2_ws/src/avena_ros2
git clone -b $GIT_BRANCH --single-branch https://gitlab.com/avenarobotics/avena_ros2 ~/ros2_ws/src/avena_ros2

touch ~/ros2_ws/src/avena_ros2/get_real_cameras_data/COLCON_IGNORE

exit 0
