#!/usr/bin/env bash
### every exit != 0 fails the script
set -e

echo "Configure Azure Kinect"

mkdir -p ~/ros2_ws/src

cd ~/ros2_ws/src
git clone -b foxy-devel https://gitlab.com/avenarobotics/Azure_Kinect_ROS_Driver
