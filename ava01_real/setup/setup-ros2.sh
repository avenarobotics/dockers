#!/usr/bin/env bash
set -e

apt update
apt install -y --no-install-recommends locales curl gnupg2 lsb-release

wget --no-check-certificate -O - https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | apt-key add -

sh -c 'echo "deb [arch=$(dpkg --print-architecture)] http://packages.ros.org/ros2/ubuntu $(lsb_release -cs) main" > /etc/apt/sources.list.d/ros2-latest.list'

apt update
apt install -y --no-install-recommends \
    python3-argcomplete \
    python3-colcon-argcomplete \
    python3-colcon-common-extensions \
    python3-pip

apt install -y --no-install-recommends \
    ros-foxy-rmw-cyclonedds-cpp \
    ros-foxy-ros-base 

apt install -y --no-install-recommends \
    ros-foxy-angles \
    ros-foxy-cv-bridge \
    ros-foxy-example-interfaces \
    ros-foxy-image-transport \
    ros-foxy-joint-state-publisher \
    ros-foxy-pcl-msgs \
    ros-foxy-pcl-conversions \
    ros-foxy-xacro

apt-get clean -y
rm -rf /var/lib/apt/lists/*
