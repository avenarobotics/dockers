#!/usr/bin/env bash
### every exit != 0 fails the script
set -e

echo "Install some common tools for further installation"

apt-get update 
apt-get install -y supervisor vim wget curl net-tools locales bzip2 mc ca-certificates less iputils-ping retry mesa-utils
apt-get clean -y
rm -rf /var/lib/apt/lists/*

locale-gen en_US.UTF-8