#!/usr/bin/env bash
### every exit != 0 fails the script
set -e

# install camera drivers
apt update
apt install libsoundio1

cd /tmp/

echo libk4a1.4 libk4a1.4/accept-eula boolean true | debconf-set-selections
echo libk4a1.4 libk4a1.4/accepted-eula-hash string 0f5d5c5de396e4fee4c0753a21fee0c1ed726cf0316204edda484f08cb266d76 | debconf-set-selections -u
dpkg -i libk4a1.4*
dpkg -i k4a-tools*

rm libk4a1.4*
rm k4a-tools*

apt clean -y
rm -rf /var/lib/apt/lists/*
