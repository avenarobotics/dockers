#!/usr/bin/env bash
### every exit != 0 fails the script
set -e

echo "Install build environment components"

apt-get update 

apt-get install -y \
    build-essential \
    cmake \
    xsltproc \
    gettext \
    git

apt-get install -y \
    libpcl-dev \
    libboost1.71-dev \
    nlohmann-json3-dev \
    libopencv-dev \
    libmysqlcppconn-dev \
    libzmq3-dev \
    mysql-client \
    libyaml-cpp-dev

apt-get autoclean -y 
apt-get autoremove -y 
apt-get clean -y
rm -rf /var/lib/apt/lists/*
