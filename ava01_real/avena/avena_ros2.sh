#!/usr/bin/env bash
### every exit != 0 fails the script
set -e
set -u

rm -rf ~/ros2_ws/src/avena_ros2
git clone -b $GIT_BRANCH --single-branch https://gitlab.com/avenarobotics/avena_ros2 ~/ros2_ws/src/avena_ros2
# git clone https://gitlab.com/avenarobotics/Azure_Kinect_ROS_Driver ~/ros2_ws/src/azure_kinect_ros_driver

touch ~/ros2_ws/src/avena_ros2/arm_controller/COLCON_IGNORE
touch ~/ros2_ws/src/avena_ros2/avena_bringup/COLCON_IGNORE
# touch ~/ros2_ws/src/avena_ros2/avena_bringup_kinect/COLCON_IGNORE
touch ~/ros2_ws/src/avena_ros2/behaviour_trees/COLCON_IGNORE
touch ~/ros2_ws/src/avena_ros2/calculate_error/COLCON_IGNORE
# touch ~/ros2_ws/src/avena_ros2/calibration/COLCON_IGNORE
# touch ~/ros2_ws/src/avena_ros2/change_detect/COLCON_IGNORE
touch ~/ros2_ws/src/avena_ros2/cli/COLCON_IGNORE
touch ~/ros2_ws/src/avena_ros2/compose_items/COLCON_IGNORE
# touch ~/ros2_ws/src/avena_ros2/custom_interfaces/COLCON_IGNORE
# touch ~/ros2_ws/src/avena_ros2/db_connector/COLCON_IGNORE
touch ~/ros2_ws/src/avena_ros2/detect_module/COLCON_IGNORE
touch ~/ros2_ws/src/avena_ros2/estimate_shape_module/COLCON_IGNORE
#touch ~/ros2_ws/src/avena_ros2/fake_joint_state_publisher/COLCON_IGNORE
touch ~/ros2_ws/src/avena_ros2/filter_detections/COLCON_IGNORE
touch ~/ros2_ws/src/avena_ros2/get_cameras_data/COLCON_IGNORE
# touch ~/ros2_ws/src/avena_ros2/get_cameras_data_bag/COLCON_IGNORE
touch ~/ros2_ws/src/avena_ros2/get_occupancy_grid/COLCON_IGNORE
# touch ~/ros2_ws/src/avena_ros2/get_real_cameras_data/COLCON_IGNORE
touch ~/ros2_ws/src/avena_ros2/generate_trajectory/COLCON_IGNORE
touch ~/ros2_ws/src/avena_ros2/grasp_debug/COLCON_IGNORE
touch ~/ros2_ws/src/avena_ros2/gripper_controller/COLCON_IGNORE
touch ~/ros2_ws/src/avena_ros2/gui/COLCON_IGNORE
# touch ~/ros2_ws/src/avena_ros2/helpers/COLCON_IGNORE
touch ~/ros2_ws/src/avena_ros2/item_select/COLCON_IGNORE
touch ~/ros2_ws/src/avena_ros2/manager_prepare_pick_and_place_data/COLCON_IGNORE
touch ~/ros2_ws/src/avena_ros2/merge_items/COLCON_IGNORE
touch ~/ros2_ws/src/avena_ros2/octomap_filter/COLCON_IGNORE
touch ~/ros2_ws/src/avena_ros2/parameters_server/COLCON_IGNORE
touch ~/ros2_ws/src/avena_ros2/path_buffer/COLCON_IGNORE
touch ~/ros2_ws/src/avena_ros2/place_debug/COLCON_IGNORE
touch ~/ros2_ws/src/avena_ros2/plugin_arm_controller/COLCON_IGNORE
touch ~/ros2_ws/src/avena_ros2/plugin_control_sim_ros2/COLCON_IGNORE
touch ~/ros2_ws/src/avena_ros2/plugin_generate_path/COLCON_IGNORE
touch ~/ros2_ws/src/avena_ros2/plugin_get_sim_cameras_data/COLCON_IGNORE
touch ~/ros2_ws/src/avena_ros2/plugin_grasp/COLCON_IGNORE
touch ~/ros2_ws/src/avena_ros2/plugin_gripper_controller/COLCON_IGNORE
touch ~/ros2_ws/src/avena_ros2/plugin_place/COLCON_IGNORE
touch ~/ros2_ws/src/avena_ros2/plugin_spawn_collision_items/COLCON_IGNORE
# touch ~/ros2_ws/src/avena_ros2/ptcld_transformer/COLCON_IGNORE
# touch ~/ros2_ws/src/avena_ros2/robot_self_filter/COLCON_IGNORE
touch ~/ros2_ws/src/avena_ros2/ros2mysql/COLCON_IGNORE
# touch ~/ros2_ws/src/avena_ros2/save_to_bag/COLCON_IGNORE
# touch ~/ros2_ws/src/avena_ros2/scene_publisher/COLCON_IGNORE
touch ~/ros2_ws/src/avena_ros2/scenes/COLCON_IGNORE
touch ~/ros2_ws/src/avena_ros2/select_new_masks/COLCON_IGNORE
touch ~/ros2_ws/src/avena_ros2/system_monitor/COLCON_IGNORE
touch ~/ros2_ws/src/avena_ros2/system_tests/COLCON_IGNORE
touch ~/ros2_ws/src/avena_ros2/validate_estimate_shape/COLCON_IGNORE
touch ~/ros2_ws/src/avena_ros2/virtualscene/COLCON_IGNORE

exit 0
