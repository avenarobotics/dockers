#!/usr/bin/env bash

exit_on_error () {
    if (( $2 > 0 ))
    then
        echo "APP:$1 ERROR CODE: $2"
        exit $2
    fi   
}

mkdir -p /opt/avena/cli

#source ~/.bashrc
source /opt/ros/foxy/setup.bash

start=`date +%s`

cd /root/install
echo "Updating ROS2 applications"

./avena_ros2.sh & ret1=$!

wait $ret1; exit_on_error "avena_ros2" $?

cd ~/ros2_ws

MAKEFLAGS="-j1" colcon build --parallel-workers 5 & ret5=$!
wait $ret5; exit_on_error "colcon build" $?

echo "Done"

end=`date +%s`
runtime=$((end-start))

echo "Building time " $runtime "s "

exit 0